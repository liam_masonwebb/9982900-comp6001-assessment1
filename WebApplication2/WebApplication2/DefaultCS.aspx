﻿<!doctype html>
<html>
<head>    
   <link href="styles.css" type="text/css" rel="stylesheet" />
   <title>liams colour changing app</title>
</head>
<body class = "main" id = "main">
    

    <div id = "top">
        <center>
        <h1>Colour changing app</h1>

        <h2>To change colour of the background please move sliders to make colours different. once you have had enough you may reset it by pusing the "make me white" button.</h2>
            </center>
        <div data-role="content" class = "slider" id = "slider"  >
            <div data-role="fieldcontain" data-controltype="slider" class="redSlider">      
               <label for="red" color="red">
                    Red
                 </label>
                <input id="red" type="range" name="redSlider" value="255" min="0" max="255"
                data-highlight="true" data-mini="true" onchange = "changeBackground()" />
            </div>
        <div data-role="fieldcontain" data-controltype="slider" class="greenSlider">
            <label for="green">
              Green
            </label>
           <input id="green" type="range" name="greenSlider" value="255" min="0"
           max="255" data-highlight="true" data-mini="true" onchange = "changeBackground()" />
        </div>
    <div data-role="fieldcontain" data-controltype="slider" class="blueSlider">
        <label for="blue">
            Blue
        </label>
        <input id="blue" type="range" name="blueSlider" value="255" min="0" max="255"
        data-highlight="true" data-mini="true" onchange = "changeBackground()" />
    </div>

<script src= "JavaScript.js"></script>

        </div>

        
        <form>
            <input type="button" align="right" value="Make Me White" onclick="window.location.href = 'DefaultCS.aspx'" />
        </form>
     </div>
  </body>
</html>